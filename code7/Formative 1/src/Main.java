public class Main {
    public static void main(String[] args){
        Gaji obj1 = new Gaji();
        Gaji obj2 = new Gaji();
        Gaji obj3 = new Gaji();
        Karyawan test1 = (Gaji) obj1;
        Karyawan test2 = (Gaji) obj2;
        Karyawan test3 = (Gaji) obj3;

        test1.setKaryawan();
        obj1.hitungGaji(1);
        obj1.thread.start();

        test2.setKaryawan();
        obj2.hitungGaji(2);
        obj2.thread.start();

        test3.setKaryawan();
        obj3.hitungGaji(3);
        obj3.thread.start();


    }
}
