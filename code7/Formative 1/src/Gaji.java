import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;

public class Gaji extends Karyawan{
    int mark;
    int[] bulan = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    ArrayList<String> formatGaji = new ArrayList<>();

    Thread thread = new Thread(){
        public void run() {
            BufferedWriter createFile = null;
            try {
                createFile = new BufferedWriter(new FileWriter("karyawan" + mark + ".txt"));
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                for(String str : formatGaji){
                    createFile.write(str + System.lineSeparator());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                createFile.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    public void hitungGaji(int mark){
        this.mark = mark;
        DecimalFormat kursIndo = (DecimalFormat) DecimalFormat.getCurrencyInstance();
        DecimalFormatSymbols formatIdr = new DecimalFormatSymbols();
        formatIdr.setCurrencySymbol("Rp. ");
        formatIdr.setMonetaryDecimalSeparator('.');
        formatIdr.setGroupingSeparator(',');

        kursIndo.setDecimalFormatSymbols(formatIdr);
        String uang = kursIndo.format(gaji);

        formatGaji.add("Nama Karyawan           : " + name);
        formatGaji.add("Tanggal masuk kerja     : " + tanggalMasuk + "-" + bulanMasuk + "-" + tahunMasuk);

        for(int i = bulanMasuk+1; i <= bulan.length; i++){
            switch (i){
                case 1 : formatGaji.add("Tanggal : 01-01-2021");
                         formatGaji.add("Gaji bulan Januari     : " + uang);
                         formatGaji.add("Monthly\n");break;
                case 2 : formatGaji.add("Tanggal : 01-02-2021");
                         formatGaji.add("Gaji bulan Februari    : " + uang);
                         formatGaji.add("Monthly\n");break;
                case 3 : formatGaji.add("Tanggal : 01-03-2021");
                         formatGaji.add("Gaji bulan Maret       : " + uang);
                         formatGaji.add("Monthly\n");break;
                case 4 : formatGaji.add("Tanggal : 01-04-2021");
                         formatGaji.add("Gaji bulan April       : " + uang);
                         formatGaji.add("Monthly\n");break;
                case 5 : formatGaji.add("Tanggal : 01-05-2021");
                         double gajiThr = gaji + (gaji * (bulanMasuk/12.0));
                         double thr = gaji * (bulanMasuk/12.0);
                         String uangGajiThr = kursIndo.format(gajiThr);
                         String uangThr = kursIndo.format(thr);
                         formatGaji.add("Gaji bulan Mei         : " + uangGajiThr);
                         formatGaji.add("THR                    : " + uangThr);
                         formatGaji.add("Monthly & Bonus THR\n");break;
                case 6 : formatGaji.add("Tanggal : 01-06-2021");
                         formatGaji.add("Gaji bulan Juni       : " + uang);
                         formatGaji.add("Monthly\n");break;
                case 7 : formatGaji.add("Tanggal : 01-07-2021");
                         formatGaji.add("Gaji bulan Juli       : " + uang);
                         formatGaji.add("Monthly\n");break;
                case 8 : formatGaji.add("Tanggal : 01-08-2021");
                         formatGaji.add("Gaji bulan Agustus       : " + uang);
                         formatGaji.add("Monthly\n");break;
                case 9 : formatGaji.add("Tanggal : 01-09-2021");
                         formatGaji.add("Gaji bulan September       : " + uang);
                         formatGaji.add("Monthly\n");break;
                case 10 : formatGaji.add("Tanggal : 01-10-2021");
                          formatGaji.add("Gaji bulan Oktober       : " + uang);
                          formatGaji.add("Monthly\n");break;
                case 11 : formatGaji.add("Tanggal : 01-11-2021");
                          formatGaji.add("Gaji bulan November       : " + uang);
                          formatGaji.add("Monthly\n");break;
                case 12 : formatGaji.add("Tanggal : 01-12-2021");
                          formatGaji.add("Gaji bulan Desember       : " + uang);
                          formatGaji.add("Monthly\n");break;
            }
        }
    }
}
