import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

public class Product {
    Scanner scan = new Scanner(System.in);
    List<Integer> pilihan = new ArrayList<>();
    HashMap<Integer, String> product = new HashMap<>();
    List<Integer> productPrice = List.of(4000, 5000, 4000, 3000, 4000,
            6000, 4000, 3000, 3000, 9000);

    public void setBuy(){
        boolean buyMore = true;
        System.out.println("---------------------Selamat Datang di IndieMart--------------------");
        System.out.println("Berikut list product yang kami jual : ");
        System.out.println("--------Nama Barang ----------------------Deskrpsi--------------------------------Harga-----------------");

        product.put(1, " Baygon          ");
        product.put(2, " Pepsodent       ");
        product.put(3, " Lifebuoy        ");
        product.put(4, " Sunlight        ");
        product.put(5, " Coca Cola       ");
        product.put(6, " Hemaviton       ");
        product.put(7, " Fanta           ");
        product.put(8, " Cheetos         ");
        product.put(9, " Indomie         ");
        product.put(10, "Fiesta Chicken  ");

        ArrayList<String> desc = new ArrayList<>();
        desc.add("");
        desc.add("Pestisida                 ");
        desc.add("Produk perawatan diri     ");
        desc.add("Produk pembersih badan    ");
        desc.add("Produk pembersih barang   ");
        desc.add("Minuman berkarbonasi      ");
        desc.add("Vitamin daya tahan tubuh  ");
        desc.add("Minuman berkarbonasi      ");
        desc.add("Makanan ringan            ");
        desc.add("Makanan cepat saji        ");
        desc.add("Frozen Food               ");

        for(int i=1; i<=product.size(); i++){
            System.out.println(i + ".       " + product.get(i) + "          " + desc.get(i) + "                  Rp. " + productPrice.get(i-1));
        }

        System.out.println("Pilih barang yang akan dibeli : ");
        while (buyMore){
            int select = scan.nextInt();
            pilihan.add(select);

            System.out.println("Ingin menambahkan produk lagi? : ");
            String more = scan.next();

            if(more.equalsIgnoreCase("Y")){
                buyMore = true;
                System.out.println("Pilih barang yang akan ditambah : ");
            } else if(more.equalsIgnoreCase("N")) {
                buyMore = false;
            }
        }

    }
}
