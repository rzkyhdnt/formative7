import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Price extends Product{
    List<Double> priceList = new ArrayList<>();

    public void getPrice(){
        try{
            int temp = pilihan.get(0);
            double disc;
            double totalBelanja = 0;
            int countBefore = 0;
            int diff = 1;

            for(int i=0; i < pilihan.size(); i++){
                priceList.add(Double.valueOf(productPrice.get(pilihan.get(i))));
            }

            Iterator checkProduct = pilihan.iterator();
            while(checkProduct.hasNext()){
                if(checkProduct.next().equals(temp)) {
                    countBefore++;
                } else {
                    diff++;
                }
            }

            if(countBefore >= 5){
                disc = priceList.get(5) - (priceList.get(5) * 0.2);
                priceList.set(5, disc);
                for(double total : priceList) totalBelanja += total;
                System.out.println("Selamat! Anda mendapatkan diskon harga barang ke-6 sebesar 20%");
                System.out.println("Total belanja anda senilai : " + "Rp. " + totalBelanja);
            } else if(diff >= 5){
                for(double total : priceList) totalBelanja += total;
                Collections.sort(priceList);
                System.out.println("Selamat! anda mendapatkan bonus 1 item dengan harga terendah senilai : " + "Rp. " + priceList.get(0));
                System.out.println("Total belanja anda senilai : " + "Rp. " + totalBelanja);
            }
        } catch(Exception e){
            System.out.println("Terjadi kesalahan : " + e.getMessage());
        }
    }
}

